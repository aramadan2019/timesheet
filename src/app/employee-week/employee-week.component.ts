import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { TaskService } from '../services/task.service';
import { ActivatedRoute } from '@angular/router';
import { ModalService } from '../services/modal.service';

@Component({
  selector: 'employee-week',
  templateUrl: 'employee-week.component.html',
  styleUrls: ['./employee-week.component.scss'],
  providers: [ModalService]
})

export class EmployeeWeekComponent implements OnInit {
  employees: any = [];
  tasks: any = [];
  employeeTasks: any = [];
  task: any = {};
  employeeId: any;
  constructor(private employeeService: EmployeeService, private modalService: ModalService,private taskService: TaskService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
    });

    this.taskService.getalltasks().subscribe(data => {
      this.tasks = data;
    });

    this.employeeId = this.route.snapshot.params['employeeId'];

    this.employeeService.getweekofEmployee(this.employeeId).subscribe(data => {
      this.employeeTasks = data;
    });
  }

  getSum(prop: string): number {
    let sum = 0;
    for (let i = 0; i < this.employeeTasks.length; i++) {
      sum += this.employeeTasks[i][prop];
    }
    return sum;
  }

  getTaskName(id: any): number {
    let task = this.tasks.find(x => x.id == id);
    return task.name;
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

  selectEmployee(event: any) {
    this.employeeTasks = [];

    this.employeeService.getweekofEmployee(this.employeeId).subscribe(data => {
      this.employeeTasks = data;
    });

  }

  saveTask() {
    this.task.EmployeeId = this.employeeId;

    this.employeeService.saveWeekOfEmployee(this.task).subscribe(res => {
      this.task = {};
      this.closeModal('custom-modal-1');

      this.employeeService.getweekofEmployee(this.employeeId).subscribe(data => {
        this.employeeTasks = data;
      });
    })
  }

}
