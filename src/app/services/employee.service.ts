import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
  private baseapi = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getallemployees() {
    return this.http.get(this.baseapi + "/employee/getall");
  }

  getweekofEmployee(id:any) {
    return this.http.get(this.baseapi + "/employee/getweekofEmployee/"+id);
  }


  saveWeekOfEmployee(task: any) {
    return this.http.post(this.baseapi + "/employee/SaveWeekOfEmployee/" , task);
  }
}
