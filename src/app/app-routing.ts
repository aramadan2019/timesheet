import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeWeekComponent } from './employee-week/employee-week.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeeListComponent
  }
  ,
  {
    path: "EmployeeWeek/:employeeId",
    component: EmployeeWeekComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
